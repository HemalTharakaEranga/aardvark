import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.junit.Test;

public class PictureFrameTest {

    @Test
    public void testFillDigitGivenCentre() {
        // Create a new BufferedImage object.
        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);

        // Get the graphics context from the BufferedImage.
        Graphics g = image.getGraphics();

        // Call the `fillDigitGivenCentre()` method.
        fillDigitGivenCentre(g, 10, 30, 20, 1);

        // Get the bounds of the rendered text.
        FontRenderContext frc = g.getFontMetrics().getFontRenderContext();
        TextLayout layout = new TextLayout("1", g.getFont(), frc);
        Rectangle2D bounds = layout.getBounds();

        // Define the expected bounds.
        Rectangle expectedBounds = new Rectangle(1, -9, 4, 9);

        // Assert that the digit is drawn correctly.
        assertEquals(expectedBounds, bounds.getBounds());
    }

    public void fillDigitGivenCentre(Graphics g, int i, int j, int k, int l) {
        // Get the digit's width and height.
        int width = 20;
        int height = 20;

        // Set the graphics' color to black.
        g.setColor(Color.black);

        // Draw a rectangle at the specified coordinates.
        g.fillRect(i - width / 2, j - height / 2, width, height);

        // Set the graphics' color to white.
        g.setColor(Color.white);

        // Draw a digit in the specified location.
        g.drawString(String.valueOf(l), i - width / 2 + 5, j - height / 2 + 10);
    }
}
